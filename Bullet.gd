extends Node2D

func _ready():
	pass
	
func _process(delta):
	position += transform.x * 2000 * delta
	
func _on_Timer_timeout():
	queue_free()

func _on_Area2D_body_entered(body):
	if body.has_method("boing"):
		body.boing(position)
		queue_free()
