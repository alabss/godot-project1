extends RigidBody2D

@export var Bullet: PackedScene
var bul_ready = true

func _ready():
	pass 

func _process(delta):
	if Input.is_action_pressed("fire"):
		if bul_ready == true:
			linear_velocity = linear_velocity + ((position - get_global_mouse_position()).normalized()*1000)
			var b = Bullet.instantiate()
			b.transform = $point.global_transform
			owner.add_child(b)
			bul_ready = false
			$Timer.start(0.5)
	


func _physics_process(delta):
	look_at(get_global_mouse_position())
	var ca = global_position
	var mid_x = (ca.x + get_global_mouse_position().x)/2
	var mid_y = (ca.y + get_global_mouse_position().y)/2
	
	var cam_pos = $Camera2D.global_position
	cam_pos = cam_pos.lerp(Vector2(mid_x,mid_y),delta)

func _on_Timer_timeout():
	bul_ready = true
